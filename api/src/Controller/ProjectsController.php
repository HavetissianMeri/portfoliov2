<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Projects;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/rest/projects", name="rest_projects")
 */
class ProjectsController extends Controller
{
    private $serializer;
   
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", methods={"GET"})
     */
    public function index()
    {
        // //On crée une réponse http
        // $response = new Response();
        // //On indique que son contenu sera du json
        // $response->headers->set("Content-Type", "application/json");
        // //On indique que le code de retour est 200 ok
        // $response->setStatusCode(200);
        // //On met comme contenu un petit tableau encodé en json
        // $response->setContent(json_encode(["ga" => "bu"]));
        
        $repo = $this->getDoctrine()->getRepository(Projects::class);
        
        //On utilise le serializer de symfony pour transfomer une
        //entité (ou array d'entités) php au format json
        $json = $this->serializer->serialize($repo->findAll(), "json");
        //On utilise la méthode static fromJsonString de la classe
        //JsonResponse pour créer une réponse HTTP en json à partir
        //de données déjà au format JSON
        return JsonResponse::fromJsonString($json);
    }
    
    /**
     * @Route("/", methods={"POST"})
     */
    public function add(Request $request) {
      
        $body = $request->getContent();
        
        $projects = $this->serializer->deserialize($body, Projects::class, "json");
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($projects);
        $manager->flush();
        
        return new JsonResponse(["id" => $projects->getId()]);
    }

    /**
     * @Route("/{projects}", methods={"GET"})
     */
    public function single(Projects $projects) {
        $json = $this->serializer->serialize($projects,"json");
        return JsonResponse::fromJsonString($json);
    }

    /**
     * @Route("/{projects}", methods={"DELETE"})
     */
    public function remove(Projects $projects) {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($projects);
        $manager->flush();
        return new Response("", 204);
    }

    /**
     * @Route("/{projects}", methods={"PUT"})
     */
    public function update(Projects $projects, Request $request) {
        $body = $request->getContent();
        $updated = $this->serializer->deserialize($body, Projects::class, "json");

        $manager = $this->getDoctrine()->getManager();

        $projects->setName($updated->getName());
        $projects->setSurname($updated->getSurname());
        $projects->setLevel($updated->getLevel());
        $projects->setTech($updated->getTech());
        
        $manager->flush();
        return new Response("", 204);
    }

    
}
