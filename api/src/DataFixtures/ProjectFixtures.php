<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\projectsList;
use App\Entity\projects;

class ProjectFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $projects = new projects();
        // $manager->persist($projects);

        $projectsList = new projectsList();
        $projectsList->setName("Test List");
        
        for ($i=0; $i < 5; $i++) { 
            $projects = new projects();
            $projects->setName("Produit ${i}");
            $projects->setTag("Food");
            $projects->setComment("This is a comment");
            $projects->setQty($i);
            $projects->setDone(false);

            $projectsList->addprojects($projects);

            $manager->persist($projects);
        }

        $manager->persist($projectsList);
        $manager->flush();
    }
}
