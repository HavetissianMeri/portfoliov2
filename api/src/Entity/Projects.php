<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectsRepository")
 */
class Projects
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $language;

    /**
     * @ORM\Column(type="text")
     */
    
    private $lienProjet;

     /**
     * @ORM\Column(type="text")
     */
    private $lienGit;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getlanguage(): ?string
    {
        return $this->language;
    }

    public function setlanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

 
     public function getLienProjet(): ?string
    {
        return $this->lienProjet;
    }

    public function setLienProjet(string $lienProjet): self
    {
        $this->lienProjet = $lienProjet;

        return $this;
    }

    public function getLienGit(): ?string
    {
        return $this->lienGit;
    }

    public function setLienGit(string $lienGit): self
    {
        $this->lienGit = $lienGit;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }


}
