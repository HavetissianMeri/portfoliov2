
import * as $ from 'jquery';

import { Projects } from "../entities/projects";

export class ProjectsRepository {
    private baseUrl = "https://www.simplonlyon.fr/promo6/mhavetissian/api/rest/projects/";
    

    findAll() {
        let url = this.baseUrl;
        let promise = $.ajax(url).then(function(response) {
            let projects = [];
            // response = JSON.parse(response);

            for (const element of response) {
                let instance = new Projects(element.title, element.language, element.lienProjet,element.lienGit, element.id);
                projects.push(instance);
            }

            return projects;
        });
        
        promise.catch(error => {
            console.error(error);
        });

        return promise;
    }

    delete(id){
        let url = `${this.baseUrl}${id}`;
        let promise = $.ajax(url, {method: "DELETE"});
        
        promise.catch(error => {
            console.error(error);
        });

        return promise;
    }

    add(projects){

        let url = this.baseUrl;
        let json = JSON.stringify(projects); 
        let promise = $.ajax(url, {method: "POST", data: json});
        
        
        promise.catch(error => {
            console.error(error);
        });

        return promise;
    
    }
  
}

