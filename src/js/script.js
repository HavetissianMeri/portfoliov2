window.sr = ScrollReveal();
sr.reveal('.skills-title, .skills-img, .section-title', {
  duration: 700,
  distance: '300px',
	origin:'bottom',
	reset: false,
	useDelay:'always',
  viewFactor: 0.8,
  
  	// this part is new!
	afterReveal: function(el) {
		console.log(el);
	}
});

window.sr = ScrollReveal();
sr.reveal('.portrait', {
  duration: 700,
  distance: '300px',
	origin:'left',
	reset: false,
	useDelay:'always',
  viewFactor: 0.9,
  
  	// this part is new!
	afterReveal: function(el) {
		console.log(el);
	}
});

window.sr = ScrollReveal();
sr.reveal('.portait-description, .project-image, .timeline', {
  duration: 700,
  distance: '20px',
	origin:'right',
	reset: false,
	useDelay:'always',
  viewFactor: 1.4,
  interval:1,

  
  	// this part is new!
	afterReveal: function(el) {
		console.log(el);
	}
});


window.sr = ScrollReveal();
sr.reveal('.social ul', {
  duration: 700,
  distance: '300px',
	origin:'top',
	reset: false,
	useDelay:'always',
  viewFactor: 1.4,
  interval:1,

  
  	// this part is new!
	afterReveal: function(el) {
		console.log(el);
	}
});