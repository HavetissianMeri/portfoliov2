
import * as $ from 'jquery';
import { LoginService } from "./services/login-service";
import { ProjectsRepository } from './repository/projectsRepository';
import { Projects } from "./entities/projects";

const template = $('#template').clone();

let loginService = new LoginService();

let service = new ProjectsRepository();

showProjets(service);
showLogin();

$('#toggle-add').on('click', () => $('#add-popup').toggle());
$('#logout').on('click', () => {
    loginService.logout();
    showLogin();
});

$('#addProjectForm').on('submit', function (event) {
    event.preventDefault();
    let errors = $('#errors').empty();
    let data = new Projects(
        $('#addProjectForm input[name="title"]').val(),
        $('#addProjectForm input[name="language"]').val(),
        $('#addProjectForm input[name="lienProjet"]').val(),
        $('#addProjectForm input[name="lienGit"]').val()

    );

    service.add(data)
        .then(Projets => showProjets(service))
        .catch(err => {
            showLogin();
            errors.text('There has been a problem')
        });

});

$('#login-form').on('submit', function (event) {
    event.preventDefault();
    let errors = $('#errors').empty();
    let username = <string>$('#username').val();
    let password = <string>$('#password').val();
    loginService.login(username, password)
        .then(() => showLogin())
        .catch(err => errors.text('Authentication error'));

});

function showLogin() {
    if (!loginService.getToken()) {
        //montrer des trucs si pas connectee
        $('#login-form').show();
        $('#add-form').hide();
        $('#logout').hide();
        $('#addProjectForm').show();
        $('#admin').hide();
        

    } else {
        //montrer des trucs si connectee
        $('#login-form').hide();
        $('#add-form').show();
        $('#logout').show();
        $('#admin').show();
    }
}


function showProjets(service) {
    $('#listProjets').empty();
    service.findAll().then(data => {

        for (const projets of data) {
            let element = template.clone();
            element.find('h4').text(projets.title);
            element.find('p').text(projets.language);
            element.find('.gitlab').attr('href',projets.lienProjet);
            element.find('.link-projet').attr('href',projets.lienGit);

            // element.find('a').attr("href", projets.lienProjet); ????????


            // let article = $('<article></article>');
            // // let h3 = $('<h3></h3>');
            // // let p = $('<p></p>');
            // // let a = $('<a>Lien vers le Projet  --</a>');
            // // let git = $('<a> Lien vers le Git  </a>');

            // let h4 =$('<h4></h4>');
            // let p = $('<p></p>');
          
            // let button = $('<button> Delete </button>');
            

            // article.addClass('col-md-4 blue carre')
            
            // p.text(projets.title);
            // h4.text(projets.language);

            // // h3.text(projets.language);
            // // h3.text(projets.language);
            // // p.text(projets.description);
            // // a.attr("href", projets.lienProjet);
            // // git.attr("href", projets.lienGit);
            
            // button.on('click', () => {
            //     service.delete(projets.id).then(() => showProjets(service));
                
            //     // rappelle la vue pour accutakise la page 
            // })
            // article.append(p);
            // article.append(h4);
            // // article.append(h3);
            // // article.append(p);
            // // article.append(a);
            // // article.append(git);
            // article.append(button);
            

            // $('#project').append(article);// ici attention c'est project et non projets
            $('#listProjets').append(element);
        }

    });
}